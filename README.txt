
CCK Multiple Formatter
----------------------

To install, place the entire cck_mulitple_formatter folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the CCK Multiple Formatter module.
On the "Display Fields" tab in "Content types" yuo will now have the new options for "Unordered list" + the original formatter

To limit which field types CCK Multiple Formatter is enabled for visit admin/settings/cck_multiple_formatter

To use the cck-multiple-formatter.tpl.php you need to copy it and content-field.tpl.php from cck/theme into your active
theme's directory.

Possible overrides for cck-multiple-formatter.tpl.php are:
      cck-multiple-formatter-[field name]
      cck-multiple-formatter-[node type]
      cck-multiple-formatter-[field name]-[node type]

A comprehensive guide to using CCK is available as a CCK Handbook at http://drupal.org/node/101723.

Maintainer
-----------
Alex Pott

