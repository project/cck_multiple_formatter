<?php

/**
 * @file
 * Provides the administration page for CCK Multiple formatter.
 */

/**
 * Administration settings page
 */
function cck_multiple_formatter_admin() {
  $form = array();
  $fields = _content_field_types();
  foreach ($fields as $id => $field) {
    $options[$id] = $field['label'];
  }
  $form['cck_multiple_formatter_field_types_disable'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Disable CCK Multiple formatter'),
    '#default_value' => variable_get('cck_multiple_formatter_field_types_disable', array('')),
    '#options' => $options,
    '#description' => t('Using CCK Multiple formatter doubles the amount of formatters available for a field. You can disable the module for a specific field types by selecting it.'),
  );
  $form['#submit'] = array('cck_multiple_formatter_admin_submit');
  return system_settings_form($form);
} 

/*
 * Submit function to clear caches after a change
 */
function cck_multiple_formatter_admin_submit($form, &$form_state) {
  //Can not be sure that the system settings submit has set this yet so set it ourselves
  variable_set('cck_multiple_formatter_field_types_disable', $form_state['values']['cck_multiple_formatter_field_types_disable']);
  //Clear the content cache
  content_clear_type_cache();
  //rebuild theme registry
  module_invoke('system', 'theme_data');
  drupal_rebuild_theme_registry();
}